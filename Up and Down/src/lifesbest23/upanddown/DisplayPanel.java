package lifesbest23.upanddown;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class DisplayPanel extends JPanel implements KeyListener{
	private static final long serialVersionUID = 4226388313919231217L;
	
	private boolean spacePressed = false;
	private BufferedImage background = new BufferedImage(300, 300, BufferedImage.TYPE_4BYTE_ABGR);
	private Ball ball = new Ball(150, 150, 5, -2, background);
	
	private int levelCounter = 0;
	
	public DisplayPanel(){
		super();
		
		this.setPreferredSize(new Dimension(300, 300));
		
		setBackground();
		
		initThread();
	}
	
	private void initThread(){
		new Thread(){
			@Override
			public void run(){
				while(true){
					if(spacePressed)
						ball.jump(60);
					ball.run();
					if(background.getRGB(ball.x/10, ball.y/10) == Color.RED.getRGB()){
						Graphics g = background.getGraphics();
						try {
							g.drawImage(ImageIO.read(this.getClass().getResource("/resources/image/level" + 
									++levelCounter +".png")), 0, 0, null);
						} catch(IllegalArgumentException e){
							try {
								g.drawImage(ImageIO.read(this.getClass()
										.getResource("/resources/image/gameEnd.png")), 0, 0, null);
							} catch (IOException e1) {e1.printStackTrace();}
						} catch (IOException e) {e.printStackTrace();}
					}
					else if(background.getRGB(ball.x/10, ball.y/10) == Color.GREEN.getRGB()){
						Graphics g = background.getGraphics();
						levelCounter = 0;
						ball.setLocation(new Point(1500, 1500));
						try {
							g.drawImage(ImageIO.read(this.getClass().getResource("/resources/image/level" + 
									levelCounter +".png")), 0, 0, null);
						} catch (IOException e) {e.printStackTrace();}
					}
					repaint();
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		}.start();
	}
	
	private void setBackground(){
		Graphics g = background.getGraphics();
		
		try {
			g.drawImage(ImageIO.read(this.getClass().getResource("/resources/image/level" + 
					levelCounter +".png")), 0, 0, this);
		} catch (IOException e) {e.printStackTrace();}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()){
		case(KeyEvent.VK_SPACE):
			spacePressed = true;
//			ball.jump(60);
			break;
		case(KeyEvent.VK_UP):
			spacePressed = true;
//			ball.jump(60);
			break;
		case(KeyEvent.VK_LEFT):
			ball.moveL(20);
			break;
		case(KeyEvent.VK_RIGHT):
			ball.moveR(20);
			break;
		}
//			spacePressed = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()){
		case(KeyEvent.VK_SPACE):
			spacePressed = false;
//			ball.jump(60);
			break;
		case(KeyEvent.VK_UP):
			spacePressed = false;
//			ball.jump(60);
			break;
		case(KeyEvent.VK_LEFT):
			ball.moveL(0);
			break;
		case(KeyEvent.VK_RIGHT):
			ball.moveR(0);
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
	
	@Override protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//draw Background
		g.drawImage(background, 0, 0, this);
		
		//draw Ball
		ball.drawBall(g, Color.BLACK);
	}

}
