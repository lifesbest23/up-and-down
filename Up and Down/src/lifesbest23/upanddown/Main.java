package lifesbest23.upanddown;

import javax.swing.JFrame;

public class Main extends JFrame{
	private static final long serialVersionUID = -5413184265311923481L;

	Main(){
		super();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Up And Down");
		
		DisplayPanel dp = new DisplayPanel();
		
		this.add( dp );
		this.addKeyListener( dp );
		
		this.pack();
		this.setResizable(false);
		
		this.setVisible(true);
	}
	
	public static void main(String[] str){
		new Main();
	}
	
}
