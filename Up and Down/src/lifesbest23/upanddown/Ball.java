package lifesbest23.upanddown;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;

public class Ball extends Point{
	private static final long serialVersionUID = -825872542483636165L;

	private int radius;
	
	private int g;
	private int v_y;	//vertical velocity
	
	private int dx_r, dx_l, dx, dx_old;
	
	private BufferedImage background;
	
	public Ball(int x, int y, int rad, int g, BufferedImage bg){
		super(x*10, y*10);
		
		this.radius = rad;
		
		this.g = g;
		this.v_y = 0;
		this.dx_r = 0;
		this.dx_l = 0;
		this.dx = 0;
		this.dx_old = 0;
		
		this.background = bg;
	}
	
	public void jump(int v){
		if(v_y == 0 && !checkOnX(y/10 + radius)){
			v_y = v;
		}
	}
	
	public void drawBall(Graphics g, Color c){
		Color temp = g.getColor();
		g.setColor(c);
		g.fillOval(x/10-radius, y/10-radius, 2*radius, 2*radius);
		g.setColor(temp);
	}
	
	public void moveR(int d){
		if(dx_r == 0)
			dx += d;
		else if(d == 0)
			dx -= dx_r;
		dx_r = d;
	}
	
	public void moveL(int d){
		if(dx_l == 0)
			dx -= d;
		else if(d == 0)
			dx += dx_l;
		dx_l = d;
	}
	
	public void moveX(int d){
		if(d != dx_old)
			dx += d;
		dx_old = d;
	}

	public void run() {
		if(v_y < 0){//downwards
			if(checkOnX((y - v_y)/10 + radius)){
				y -= v_y;
				v_y += g;
			}
			else if(checkOnX(y/10 + radius)){
				while(checkOnX(y/10 + radius))
					y += 10;
			}
			else
				v_y = 0;
		}
		else if(v_y > 0){//Upwards
			if(checkOnX((y - v_y)/10 - radius)){
				y -= v_y;
				v_y += g;
			}
			else if(checkOnX(y/10 - radius)){
				while(checkOnX(y/10 - radius))
					y -= 10;
			}
			else
				v_y = 0;
		}
		else if(v_y == 0){
			if(g < 0){
				if(checkOnX(y/10 + radius)){
					v_y += g;
				}
			}	
			else if(g > 0){
				if(checkOnX(y/10 - radius)){
					v_y += g;
				}
			}
		}
		if(dx < 0){//Left
			if(checkOnY(x/10 - radius)){
				x += dx;
			}
		}
		else if(dx > 0){//Right
			if(checkOnY(x/10 + radius)){
				x += dx;
			}
		}
		
	}
	
	private boolean checkOnX(int y){
		if(y > background.getHeight() || y < 0 ||
				x/10 + radius > background.getWidth() || x/10 - radius < 0)
			return false;
		for(int i = x/10 - radius + 1; i < x/10 + radius - 2; i++)
			if(background.getRGB(i, y) == Color.black.getRGB())
				return false;
		return true;
	}
	
	private boolean checkOnY(int x){
		if(y/10 + radius > background.getHeight() || y/10 - radius < 0 ||
				x > background.getWidth() || x < 0)
			return false;
		for(int i = y/10 - radius + 1; i < y/10 + radius - 2; i++)
			if(background.getRGB(x, i) == Color.black.getRGB())
				return false;
		return true;
	}

}
